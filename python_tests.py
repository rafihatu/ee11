"""
We'll take a look at pytest and unittest.
1. Create a python package. It is important that the name of your package is `test` or `tests`
2. All test files must start with the name `test`.
- Pytest:
    - All functions in your test file should equally start with the name `test`.
    - You can have multiple assertions in one test function.
    - test function names should imply what is being tested. e.g "test_when_then" -> then_when_sides_are_the_same_then_triangle_is_equilateral
    - test not just for happy cases but for error-prone cases as well.
    - pytest fixtures are used to define shared resources amongst test functions.
    - Run pytest using the command - `pytest` or `python -m pytest`
    - Run unnitest using the command - `python -m unittest`
"""

"""
In writing unittests, you need to do the following:
- Import the unittest module's TestCase
- Import the module you want to test.
- Create a test class and inherit unittest TestCase
assert (True, False, IsNotNone, IsNone, Raises, Equal, AlmostEqual, Greater, GreaterEqual, Less, LessEqual, In)
"""