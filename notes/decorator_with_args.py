import datetime
from functools import wraps
from decorator_classes import PreventSpies
from dataclasses import dataclass


class ContextManager:
    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode

    def __enter__(self):
        self.file = open(self.filename, self.mode)
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()


time = datetime.datetime.now().replace(microsecond=0)


class Logger:

    @staticmethod
    def log(time, username, result):
        with ContextManager('log.txt', 'a') as file:
            file.write("User named {} tried to login at {}. Result was {}!".format(username, time, result))


def prevent_spies(spy_type):
    def inner_func(func):
        @wraps(func)
        def wrapper(username, password):
            """
            decorator function to validate login function
            :return: validated login function
            """
            students = {"raivo": "sulla", "joonas": "ole", "anneli": "silbaum", "siim": "kaevats"}

            if isinstance(username, str) and isinstance(password, str):
                if username == username.lower() and password == password.lower():
                    if username in students and password == students[username]:
                        Logger.log(time, username, "LOGIN SUCCESS")
                        return func(username, password, last_login=time)
            Logger.log(time, username, "ERROR")
            return "ERROR!"
        return wrapper

    print(spy_type)
    return inner_func


@dataclass()
class Person:
    username: str
    password: str
    last_login: datetime.datetime = datetime.datetime.now()


# @prevent_spies(spy_type="hacker")
# @PreventSpies
def login(username, password, last_login=None) -> Person:

    """
    Complete this function so that a student logs in to the class.
    Note that username and password should be accepted in lowercase only.
    :return: "LOGIN SUCCESS" or "ERROR"
    """
    print("LOGIN SUCCESS!")
    return Person(username=username, password=password)


# from timeit import timeit
#
# stmt = 'prevent_spies(spy_type="hacker")(login)("anneli", "silbaum")'

#
# print(timeit(setup=setup, stmt=stmt, number=100))
user = login("anneli", "silbaum")
print(user.last_login)
#
# import cProfile
#
# cProfile.run(statement=stmt)
