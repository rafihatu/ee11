# keywords =
# False	else import	pass
# None	break	except	in	raise
# True	class	finally	is	return
# and	continue	for	lambda	try
# as	def	from	while
# assert	del	global	not	with
# elif	if	or	yield
# -------------------------------------


variable = "A random text"
# == Equal to. if a == b. Checks that a and b are the same value.
# != Not equal. Checks that two objects aren't the same. if a != b
# > Checks if the first object is greater than the second e.g If 245 > 5
# >= Checks if the first object is greater than or the same as the second e.g If 245 >= 5
# < Checks if the first object is less than the second e.g If 25 < 5
# <= Checks if the first object is less than or the same as the second e.g If 245 <= 5
# `and` An example would be:  If (trainer == "Rafi") and (students.count() >= 16) and students.active and internet.good: start class
# `or` An example would be:  If (trainer == "Rafi") or (admin == "Elina"): register students
# `in` An example would be: if "Student12" in "We have many students in our class but we love Student12 the most." :
# `not in` An example would be: if "Student12" not in ["Student1", "Student2", "Student3", "Student4", "Student5", "Student6", "Student7", "Student8"]:
# `is not` An example would be: if student.present is not True: "student is absent"

# ...........................Code Starts here...........................

from dict_functions import class_info, student1, student2
from list_functions import students_list, extra_list
# print(students_list)
# print(extra_list)

# Description: Attendance of students as they join the zoom meeting
attendance_list = []

# Anneli joins via the link:
attendance_list.append("Anneli")

# Germaine joins via the link:
attendance_list.append("Germaine")

# Kadri joins via the link:
attendance_list.append("Kadri")

# Raivo joins via the link:
attendance_list.append("Raivo")

# Joonas joins via the link:
attendance_list.append("Joonas")

# Raivo leaves the meeting:
# Raivo joins the meeting again via the link:
attendance_list.append("Raivo")

# Kadri leaves the meeting:
# Kadri joins again via the link:
attendance_list.append("Kadri")


# How many students do we now have in class?

if ("Kristina" in attendance_list) and ("Sarbar" in attendance_list) and ("Kristo" in attendance_list):
    print("All three jolly friends are present in class")
else:
    pass
    # print("The three jolly friends are either not together in class or not in class at all")


# Looking at the previous question that handles attendance of students:
# 1. While the attendance list does not contain 17 students: Prompt the user to give us a name.
# Once the attendance list has 17 unique names, stop prompting the user for a name.
# Use the builtin input function.
