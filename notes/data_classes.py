from dataclasses import dataclass, field, asdict, astuple


@dataclass(unsafe_hash=True, frozen=True, init=True)
class Person:
    name: str = field(hash=True)
    eye_color: str
    hair_color: str
    legs: int
    arms: int
    age: int
    nationality: str

    def sample_method(self):
        return f"{self.name} is a {self.nationality} and is {self.age} years old."


rafi = Person("Rafihatu", "black", "black", 2, 2, 2, "Worldwide")
lizy = Person("Elizabeth", "black", "black", 2, 2, 2, "Worldwide")
sam = Person("Samuel", "black", "black", 2, 2, 2, "Worldwide")
anna = Person("Anna", "black", "black", 2, 2, 2, "Worldwide")

all_people = [rafi, sam, lizy, anna]
