text = """ \n \n \n
This is the appended text.
This module talks about basic file operations in python.
There are three most common modes of file operations:
    1. Read "r"
    2. Write "w"
    3. Append "a"
For read operations:
- read(): Reads the entire file
- readline(): Reads a single line
- readlines(): Reads all lines and returns a list
- extraline for writelines operation.
open and close are the most common operations.
use `with` to open and close the file.
"""

# file = open("example.csv", "r")
# print(file.readlines())
# file.close()

with open("example.csv", "r") as file:
    print(file.read())

print(file.readlines())


# try:
#     file = open("example.csv", "r")
# except Exception as error:
#     raise error
# else:
#     print(file.read())
# finally:
#     file.close()
