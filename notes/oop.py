import datetime


class Person:
    def __init__(self, name, age, eye_color, hair_color, nationality) -> None:
        self.eye_color = eye_color
        self.hair_color = hair_color
        self.legs = 2
        self.arms = 2
        self.name = name
        self.age = age
        self.nationality = nationality

    def __iter__(self):
        return iter([self.name, self.age, self.eye_color, self.hair_color, self.nationality, self.legs, self.arms])

    def walking(self):
        pass

    def eating(self):
        pass

    def sleeping(self):
        current_time = self.__get_current_time()
        if current_time > "22" or current_time < "07":
            return f"{self.name} is sleeping"
        else:
            return f"{self.name} is awake and his nationality is"

    def greeting(self):
        current_time = self.__get_current_time()
        if current_time < "12":
            return "Good morning"
        elif current_time >= "12" and current_time < "17":
            return "Good afternoon"
        else:
            return "Good evening"

    def studying(self):
        pass

    def playing(self):
        pass

    def working(self, job):
        current_time = self.__get_current_time()
        if current_time >= "09" and current_time <= "17":
            return f"{self.name} is probably working right now."
        else:
            return f"{self.name} is most likely not working right now."

    def __get_current_time(self):
        current_date_and_time = str(datetime.datetime.now()).split()
        current_time = current_date_and_time[1]
        current_time = current_time[:2]
        return current_time

    def __repr__(self) -> str:
        return f"{self.name}, {self.age}"

# anneli = Person(name="Anneli", age=22, hair_color="brown".capitalize, eye_color="black", nationality="Estonian")
# madis = Person(name="Madis", age=32, hair_color="blonde", eye_color="blue", nationality="Estonian")


# print(madis.greeting())


class Student(Person):
    def teaching(self):
        return f"{self.name} is a student not a teacher.".capitalize

    def exercising(self):
        current_time = self.__get_current_time()
        if current_time >= "05" and current_time <= "10":
            return f"{self.name} is probably exercising right now."
        return f"It's past exercising hours \n{self.name} is most likely not exercising right now."

    def __get_current_time(self):
        current_date_and_time = str(datetime.datetime.now()).split()
        current_time = current_date_and_time[1]
        current_time = current_time[:2]
        return current_time

    # def working(self):
    #     return f"{self.name} does not work."


# student_madis = Student(name="Madis", age=32, hair_color="blonde", eye_color="blue", nationality="Estonian")

# print(student_madis.exercising())


class HealthyEstonian:
    def __init__(self, health) -> None:
        self.health_level = health
        # self.name = person.name
        # self.person = person
        # self.age = person.age

    def test_for_covid(self, person):
        return f"An {person.nationality} aged {person.age} just got tested for covid. \n{person.name} is healthy. \n{person.sleeping()}"


# estonian = HealthyEstonian(health=100) 
# person_obj = Person(name="Anneli", age=22, hair_color="brown".capitalize, eye_color="black", nationality="Estonian")
# print(estonian.test_for_covid(person=person_obj))
# print(estonian.person.sleeping())

class SDAStudent(HealthyEstonian, Student):
    def __init__(self, name, age, eye_color, hair_color, nationality, classroom, health):
        Student.__init__(self, name=name, age=age, eye_color=eye_color, hair_color=hair_color, nationality=nationality)
        HealthyEstonian.__init__(self, health)
        self.classroom = classroom

    def attending_class(self, class_topic):
        if class_topic.lower() == "git and gitlab":
            return "Not attending"
        return "Attending"



sda_student = SDAStudent(name="Madis", age=32, hair_color="blonde", eye_color="blue", nationality="Estonian", health=99.99, classroom="Python Class")

# print(sda_student.name)


class Festival:
    def __init__(self, **people):
        self.attendees = people
        self.first_person = self.attendees.get("anneli")



anneli = Person(name="Anneli", age=22, hair_color="brown".capitalize, eye_color="black", nationality="Estonian")
sarbar = Person(name="Sarbar", age=22, hair_color="brown".capitalize, eye_color="black", nationality="Estonian")
raivo = Person(name="Raivo", age=102, hair_color="brown".capitalize, eye_color="black", nationality="Estonian")
festival = Festival(anneli=anneli, sarbar=sarbar, raivo=raivo)


# # print(raivo)
# print(festival.attendees)


# {"anneli": "Anneli"}