from main import example_dict as class_info
import pprint


student1 = {
    "first_name": "Student",
    "last_name": "One",
    "date_enrolled": "05/03/2022",
    "percentage_presence": "100%",
    "last_active": "Today",
    "birthday": "12/12/2012",
    "is_online": False
}

student2 = {
    "first_name": "Student",
    "last_name": "Two",
    "date_enrolled": "05/03/2022",
    "percentage_presence": "50%",
    "last_active": "Today",
    "birthday": "12/12/2002",
    "is_online": True
}

# 2. As from python 3.7, dictionaries are ordered. In lower python versions, dictionaries are unordered.
# Dictionary values can be gotten from calling bracket notation on their keys.
# print(student1["date_enrolled"])

# Get the length of a dict:
# print(len(student1))

# 3. Dict values can be changed.
# student1["is_online"] = True
# student1["last_name"] = "New name"


# 4. You can add new values to a dictionary.
student1["username"] = f"{student1['first_name']} {student1['last_name']}"


# 5. Values within a dict can be accessed by their keys.
# print(student1["username"])
# print(student1.get("username"))

# 6. Dictionaries can contain various datatypes as values.
# print(class_info)
# first_student = class_info["students"][0]

class_info["students"][0] = student1
class_info["students"][1] = student2


# 7. Dictionaries do not allow duplicate keys but values can be duplicate.

# 8. Remove items from dictionary:
# class_info.pop("class")
# class_info.popitem()

# You can use the `del` keyword to delete things
# del class_info
# pprint.pprint(class_info)

# 9. Commonly used dictionary functions
# - items()
# print(class_info.items())

# - keys()
# print(list(class_info.keys()))

# - values()
# print(list(class_info.values()))
# [("class", "EE11"), ("trainer", "Rafihatu Bello"), ("students", )]