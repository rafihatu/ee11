# In python, we have two types of loops:
# - For loops
# - While loops


# `For` loops can be used on sequences / iterables in python:
# Example iterables are strings, lists, tuples, sets, dictionaries.

# students_list = ["Student1", "Student2", "Student3", "Student4",
#                  "Student5", "Student6", "Student7", "Student8",
#                  "Student9", "Student10", "Student11", "Student12"]

# for i in students_list:
#     print(i)
    # print(students_list)
# print("Loop completed")

# length = len(students_list) #12

# (0,1,2,3,4,5,6,7,8,9,10,11)
# for i in range(length):
#     if i == 3:
#         continue
#     else:
#         students_list[index] = f"{students_list[index]} updated"
# print(students_list)
# #
#
# # range()
# # range(0,5)
# print(students_list)

# index slicing = [start: stop: step]
# range = (start, stop, step)


# print(10 % 2)


# The difference between continue and break:
# Continue tells the loop to move on without it
# Break means stop


# While Loops
# switch = True
# while switch:
#     print(length)
#     switch = False
#     students_list.append("Additional Student")
# print(f"While loop has ended. Number of students now {len(students_list)}", students_list)




# Try, except, else, finally in python: These are error handling keywords
from functions import get_students_attendance

get_students_attendance()