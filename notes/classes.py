"""
OOP - Object Oriented Programming.
There are two major characteristics of objects in OOP.
- attributes: think of them as variables
- behaviour/methods: think of them like functions.
When defining functions within classes (known as class methods), you should use the self keyword as the first parameter in the function
e.g: def start_studies(self): pass
class methods can take in as many arguments as possible.
class attributes / instance attributes can be accessed by all methods within the class.
To access class / instance attributes, you also need to use the self keyword.
e.g if a variable is named `age`, you use the age variable in other parts of the code as `self.age`
"""


# class HumanBeing:
#     def start_walking(self):
#         return "Hey guys! I can walk. I am walking very slowly."
#
#     def start_eating(self):
#         return "Hey guys! I can eat. I am eating very slowly."
#
#     def go_to_school(self, name_of_school):
#         return f"Hey guys! I school at {name_of_school}. I am learning very slowly. I don't seem to like school"


# human_object = HumanBeing()
# print(human_object.go_to_school("Software Development Academy"))



class SDA:
    # example = "This is an example of class attributes"
    def __init__(self):  
        self.__trainer = "rafihatu"
        self.students = []
        self.venue = "Zoom"
        self.attendance_list = []

    def add_new_student(self, student_name: str, *args, **kwargs):
        self.__trainer_adds_student(student_name)
        self.students.append(student_name)
        print(f"Total number of students in class now {len(self.students)}")
        return self.students

    def start_class_session(self, topic_of_the_day):
        self.example

    def take_class_attendance(self, name: str) -> bool:
        """
        Implement the function that takes the attendance of the students.
        Check that a student is part of our students list.
        - If the students is a part of the students_list, add the student's name to the attendance.
        - If the student is not a part of our student list, add the student to both the student list and attendance_list
        :return: True or False based on if you newly added the student to student_list or not.
        """
        name = name.capitalize()
        if name in self.students:
            self.attendance_list.append(name)
            return False
        else:
            self.students.append(name)
            self.attendance_list.append(name)
            return True

    def give_assignment(self):
        pass

    def __trainer_adds_student(self, student_name: str): 
        print(f"The trainer {self.__trainer.capitalize()}, has added a new student with name {student_name}")
        


# sda_obj = SDA()
# print(sda_obj.add_new_student("Mark"))
# print(sda_obj.add_new_student("Anneli"))
# print(sda_obj.take_class_attendance("Mark"))
#


# first_sda = SDA()
# second_sda = SDA()
#
# print("First SDA students are:", first_sda.students)
#
# print("Second SDA students are:", second_sda.students)



"""
Class variables are variables that can be accessed and manipulated by all instances/objects of that class.
Instance variables are variables that can only be accessed by the instance and are peculiar to that instance/object.
Class variables are simply defined after the class has been defined. 
Class variable definition are straight forward just like defining a basic python variable.
Instance variables are created inside the __init__ method.
Instance variables are defined with the `self` keyword
methods that have two underscores before and after them are called magic methods or dunder (double underscore) methods.
Private attributes and methods are defined with two underscores before the name e.g self.__name = name, def __validate_number(): pass
"""



#
# class Person:
#     def __init__(self, name):
#         self.name = name
#         self.hobbies = []
#
#     def get_name(self):
#         return f"My name is {self.name}"
#
#     def change_name(self):
#         self.name = self.name + " extra"
#         return f"My name was recently changed. My new name is {self.name}"
#
#     def add_new_hobbies(self, new_hobby):
#         self.hobbies.append(new_hobby)
#         return self.hobbies
#
#
# rafi = Person("Rafihatu")
# rafi.add_new_hobbies("Singing")
# rafi.add_new_hobbies("Dancing")
# # print(rafi.hobbies, "Rafi")
#
#
# siim = Person("Siim")
# siim.add_new_hobbies("Hiking")
# siim.add_new_hobbies("Horse Riding")
# # print(siim.hobbies, "Siim")
#
#
# raivo = Person("Raivo")


# print(rafi.hobbies, "Rafis hobbies")
# print(siim.hobbies, "Siims hobbies")
# print(raivo.hobbies, "Raivos hobbies")



#
# first_number = 5
# second_number = 10
#
#
# print(first_number + second_number)
#
# first_number.__add__(second_number)





import datetime
from word2number import w2n


class Car:
    def __init__(self, brand, year_created):
        self.brand = brand
        self.age = datetime.date.today().year - year_created

    def get_mileage(self, total_distance_covered_by_vehicle):
        return total_distance_covered_by_vehicle - self.age


# honda = Car("Honda", 1998)
# honda.get_mileage(20)
# print(honda.age)
#
# hyndai = Car("Hyndai", 2016)
# print(hyndai.age)
#
# toyota = Car("Toyota", 2020)
# print(toyota.age)






"""
Class Task:
Create a calculator class. Your calculator class should have a default value.
Your calculator class should have methods `add, subtract, divide and multiply`. 
- Each method should perform their name.
- Each method should return the default calculator value after the calculation has been done.
The calculator class should also contain a method for resetting the calculator's value to zero.
Bonus points if you add error handling techniques, use type hints and write docstrings.

For better understanding, I have added a codebase for you to implement further.
"""
from math import sqrt


class Calculator:
    """
        This is a simple Calculator that performs basic addition, subtraction,
        division, and multiplication of numbers.
    """

    def __init__(self):
        self.__current_value = 0

    def find_exponential(self, number):
        self.__current_value = pow(self.__current_value, number)
        return self.__current_value

    def add(self, num):
        try:
            if isinstance(num, str):
                if num.isdigit():
                    num = int(num)
                else:
                    num = w2n.word_to_num(num)
            self.__current_value += num
        except (ValueError, TypeError, NameError) as error:
            raise error
        else:
            return self.__current_value

    def divide(self):
        num = int(input("What number would you like to divide by? "))
        num_is_valid = self.__validate_number(num)
        if num_is_valid:
            self.__current_value /= num
            format = input("What format would you like to get your result? (str or num)")
            return self.__current_value if format == "num" else str(self.__current_value)
        return "Not allowed to divide by zero."

    def subtract(self, num):
        self.__current_value -= num
        return self.__current_value

    def multiply(self, num):
        self.__current_value *= num
        return self.__current_value

    @staticmethod
    def __validate_number(num):
        return False if num == 0 else True

    def reset_value(self):
        self.__current_value = 0
        return self.__current_value
