"""
For a class to be called an abstract class, the class needs to inherit from ABC.
You inherit the ABC from abc module.
Specify all methods that you want to be compulsory in Child classes and decorate them with `@abstractmethod`.
"""
from abc import ABC, abstractmethod

#
# class Polygon(ABC):
#     @property
#     @abstractmethod
#     def first(self):
#         pass
#
#
#     @abstractmethod
#     def calculate_area(self):
#         return "polygon area calculated."
#
#     @abstractmethod
#     def number_of_sides(self):
#         pass
#
#
# class Decagon(Polygon):
#     first = 3
#     second = 4
#     third = 5
#
#     def calculate_area(self):
#         print("My area is 100.")
#
#     def number_of_sides(self):
#         print("I have 10 sides.")
#
#
# class Triangle(Polygon):
#     def calculate_area(self):
#         print("My area is 150.")
#
#     def number_of_sides(self):
#         print("I have 3 sides.")
#
#     def get_height(self):
#         print("My height is fake.")
#
# deca = Decagon()
# deca.calculate_area()

# polygon = Polygon()
# print(polygon.calculate_area())



# class PersonIntroduction:
#     def get_name(self, name):
#         return f"My name is {name}."
#
#     def get_hobbies(self, hobbies):
#         return f"My hobbies are: {hobbies}."
#
#     def get_further_details(self, details):
#         return f"Other things you should know about me are: {details}."


class Visualization:
    def __init__(self, name):
        self.name = name
    # @abstractmethod
    # def get_legend(self):
    #     raise NotImplementedError
    #
    # @abstractmethod
    # def get_x_axis(self):
    #     raise NotImplementedError
    #
    # @abstractmethod
    # def get_y_axis(self):
    #     raise NotImplementedError
    #
    # @abstractmethod
    def get_title(self):
        print("I am a title method.")

    @classmethod
    def show_plot(cls, name):
        obj = cls(name)  # -> Visualization(name)
        return obj.get_title()
        # raise NotImplementedError


class PieChart(Visualization):
    def get_x_axis(self):
        print("_________________________________")

# PieChart()
pie_chart = Visualization.show_plot("Any Plot")
