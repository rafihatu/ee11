from math import sqrt


def prime_generator(n):
    # Generator for iterating over n primes
    number = 2
    generated_numbers = 0
    while generated_numbers <= n:
        if is_prime(number):
            yield number
            generated_numbers += 1
        number += 1


def is_prime(n):
    for i in range(2, int(sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True


def even_generator(n):
    # Generator for iterating over 'n' even numbers.
    number = 1
    generated_numbers = 0
    while number <= n:
        if number % 2 == 0:
            yield number
            generated_numbers += 1
        number += 1


even = even_generator(10)
# print(next(even))

even_list = (num**2 for num in range(2, 11, 2))
print(tuple(even_list))


"""
You are a boxing coach in a school (SDA) and your school is in a 
boxing competition with another school (DAS).
Implement an iterator either by creating your own Iterator class or a generator function 
that tells us the next student of your school that would fight in the next round.
"""