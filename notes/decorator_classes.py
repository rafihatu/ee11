from functools import wraps

class PreventSpies:
    def __init__(self, func):
        self.function = func

    def __call__(self, username, password):
        students = {"raivo": "sulla", "joonas": "ole", "anneli": "silbaum", "siim": "kaevats"}

        if isinstance(username, str) and isinstance(password, str):
            if username == username.lower() and password == password.lower():
                if username in students and password == students[username]:
                    return self.function(username, password)
        return "ERROR!"
