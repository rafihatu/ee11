"""
The syntax for a lambda function is: lambda arg: return_value
if the lambda function should take more than 1 argument, the syntax is:
lambda first_arg, second_arg, third_arg: return_value
"""

convert_to_lower = lambda word, opposite: f"Word: {word}, Opposite: {opposite}".lower()
print(convert_to_lower("Software Developers Rock!", "Is it true?"))

# def get_square(number):
#     return number ** 2

items = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

even_squared = list(map(lambda number: number ** 2 if number % 2 == 0 else None, items))

print(even_squared)

def get_second_value(details: tuple):
    return details[1]

students_and_ages = [("Anneli", 30), ("Sarbar", 25), ("Raivo", 12), ("Germaine", 19),
                     ("Randar", 44), ("Kalai", 32), ("Madis", 50), ("Mariel", 24)]

sorted_students = sorted(students_and_ages, key=lambda student_record: student_record[1])

print(sorted_students)


"""
Using map and lambda, write the code that returns the file extension from a list of files e.g 
[“example.txt”, "words_and_opposite.csv", "files.py", "javascript.json"]
should return [".txt", ".csv", ".py", ".json"]
"""


columns = ['pk', 'event_date', 'event_timestamp', 'event_name',
'event_previous_timestamp', 'event_value_in_usd',
'event_bundle_sequence_id', 'event_server_timestamp_offset', 'user_id',
'user_pseudo_id', 'privacy_info', 'user_first_touch_timestamp',
'user_ltv', 'device', 'geo', 'app_info', 'traffic_source', 'stream_id',
'platform', 'event_dimensions', 'ecommerce', 'ga_session_id',
'engaged_session_event', 'entrances', 'firebase_screen_id',
'firebase_screen_class', 'firebase_event_origin', 'ga_session_number',
'engagement_time_msec', 'session_engaged', 'firebase_conversion',
'previous_app_version', 'firebase_screen', 'message_type',
'firebase_previous_class', 'firebase_previous_id',
'firebase_previous_screen', 'item_id', 'item_name', 'item_category',
'currency', 'price', 'brand_id', 'quantity', 'category_id',
'category_name', 'value', 'recommended_items_view_type', 'brand_name',
'checkout_step', 'weekday', 'time_id', 'day_id', 'pickup_station',
'checkout_option', 'section', 'filled_email',
'checked_receive_offers_sms', 'language_code', 'filled_password',
'checked_receive_offers_email', 'checked_opt_in_offers',
'filled_first_name', 'filled_phone_number',
'checked_receive_offers_in_app', 'filled_last_name', 'method',
'previous_os_version', 'previous_first_open_count',
'update_with_analytics', 'region', 'type', '_ltv_EUR', '_ltv_PLN',
'firebase_exp_4', 'firebase_exp_5', 'firebase_exp_6', 'firebase_exp_7',
'first_open_time', 'last_advertising_id_reset', 'prevenue_28d']