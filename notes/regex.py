import re


number1 = "+44(0) 34567 8901"
number2 = "+44345678901"
number3 = "0345678901"
number4 = 44345678901


def is_valid_phone_number(number: str):
    if number.startswith("+") or number.startswith("+44(0)") or number.startswith("+44 (0) "):
        return True


# wrong = "1234rafi@gmail.1com1"
email = "Raaaaaafihatu_bello15@gmail.com"

# changed_email = re.sub("ager", "yahoo", email)
# domains = re.findall("\s", email)
# searched = re.search("@.*", email)
# splitted = re.split("@", email)
matched = re.search("@.{5}", email)
# print(matched.group())


class Util:
    @staticmethod
    def is_valid_email(email):
        # print("Valid email function started")
        pattern = r"^[a-zA-Z][a-zA-Z0-9.#,$,%,&]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}"
        match = re.search(pattern, email)
        # print("Valid email function ended")
        return True if match else False


    @staticmethod
    def is_valid_password(password):
        """
        checks that a password contains at least one uppercase,
        one lowercase, one special character, one number and
        must be at least 8 characters long.
        """
        # print("valid password function started")
        pass_regex = r"(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*]).{8,}"
        match = re.match(pass_regex, password)
        # print("valid password function ended.")
        return True if match else False
