import datetime



class Person:
    def __init__(self, name, age, eye_color, hair_color, nationality) -> None:
        self.eye_color = eye_color
        self.hair_color = hair_color
        self.legs = 2
        self.arms = 2
        self.name = name
        self.age = age
        self.encoded_name = Utils.encode_username(name)
        self.nationality = nationality
        self.otp_code = Utils.generate_otp()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __iter__(self):
        return iter([self.name, self.age, self.eye_color, self.hair_color, self.nationality, self.legs, self.arms])

    def walking(self):
        pass

    def eating(self):
        pass

    def sleeping(self):
        current_time = self.__get_current_time()
        if current_time > "22" or current_time < "07":
            return f"{self.name} is sleeping"
        else:
            return f"{self.name} is awake and his nationality is {self.nationality}"

    def greeting(self):
        current_time = self.__get_current_time()
        if current_time < "12":
            return "Good morning"
        elif current_time >= "12" and current_time < "17":
            return "Good afternoon"
        else:
            return "Good evening"

    def studying(self):
        pass

    def playing(self):
        pass

    def working(self, job):
        current_time = self.__get_current_time()
        if current_time >= "09" and current_time <= "17":
            return f"{self.name} is probably working right now."
        else:
            return f"{self.name} is most likely not working right now."

    def __get_current_time(self):
        current_date_and_time = str(datetime.datetime.now()).split()
        current_time = current_date_and_time[1]
        current_time = current_time[:2]
        return current_time

    @staticmethod
    def get_current_time():
        current_date_and_time = str(datetime.datetime.now()).split()
        current_time = current_date_and_time[1]
        current_time = current_time[:2]
        return current_time

    def __repr__(self) -> str:
        return f"{self.name} - {self.nationality}"


import base64
import random


class Utils:
    @staticmethod
    def encode_username(name):
        return base64.b64encode(b"{name}")

    @staticmethod
    def generate_otp():
        return random.randrange(1000, 9999)

    @staticmethod
    def convert_seconds_to_minutes(duration: int or float):
        return duration // 60


# rafi = Person("Rafihatu", "20", "brown", "red", "Angolan")
# print(rafi)
