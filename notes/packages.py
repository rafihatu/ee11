"""
1. Virtual Environments 
    - Create the venv folder in the same directory as the python file.
    - Run the following command in the venv folder:
        python -m venv venv
    - Activate the venv by running the following command:
        source venv/bin/activate
    - Run the following command to deactivate the venv:
        deactivate
2. requirements.txt
3. pip 
Remember:
Do not push your virtual environment to github, gitlab, bitbucket etc.
Always add a requirements.txt file in the root directory of your project
and push that instead.
To view packages on your terminal, run the following command:
- pip freeze
To save your requirements into the requirements.txt file, run the command:
- pip freeze > requirements.txt
"""
def create_new_column(row):
    return f"{row.first_name} {row.last_name}"

import pandas as pd


dataframe = pd.read_csv("example.csv")
dataframe["full_name"] = dataframe['first_name'] + " " + dataframe['last_name']

print(dataframe.tail())
