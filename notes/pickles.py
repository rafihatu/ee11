import csv
import json
import pickle
from student_scores import student_scores
from cls_and_static import Person


with Person("Rafihatu", "20", "brown", "red", "Angolan") as rafi:
    print(rafi.sleeping())

# pickle_string = pickle.dumps(student_scores)

# with open("example.pickle", "rb") as pickle_file:
#     print(pickle.load(pickle_file))
#     print("........SUCCESS........")

# print("context manager has been closed.")


# with open("example.csv", "a") as csv_file:
#     content = csv.writer(csv_file)
#     content.writerow(["Example", "Class", "103", "existing"])

# with open("example.json", "a") as json_file:
#     # json.dump(student_scores, json_file, indent=4)
#     json.dump({"try234": "except"}, json_file, indent=4)
#     json_file.write(",")
#     print("Successful!")

# print("Json content", content)
# print(f"Datatype is: {type(json.dumps(student_scores))}")
