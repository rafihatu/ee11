# Python Statements, Comments and Indentation
# Always name variables, functions and files using snake_case.
# The basic datatypes in Python are: strings, numbers(integers, float, complex), list, dictionary, tuples, sets, boolean
# Converting from one datatype to another in python is called "casting". You can equally call it conversion.
# The major difference between a tuple and a list is that tuples use regular brackets () while lists use square brackets []
# The major similarity between sets and dictionaries is that they are both contained in curly brackets {}

# students_list = ["Student1", "Student2"]
# teacher = "Rafihatu Bello"
# students_tuple = ("Student1", "Student2")
#
# example_dict = {
#     "class": "Estonia 11",
#     "trainer": teacher,
#     "students": students_list,
# }
#
# example_set = {"Student1", "Student2", "Student1", "Student3"}
#
# bool = True, False

# print(len(example_set))
from triangle import Triangle

triang = Triangle(3, 4, 5)
print(triang.is_equilateral())
