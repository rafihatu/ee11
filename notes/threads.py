from threading import Thread
from regex import Util


# first_thread = Thread(target=Util.is_valid_email, args=("rafi.bello@yahoo.com",))
#
# second_thread = Thread(target=Util.is_valid_password, args=("Python",))  # writing the characters of the string

# starting threads
# first_thread.start()
# second_thread.start()
#
# # waiting until both threads have finished executing before executing further code
# first_thread.join()
# second_thread.join()
#
# print("Done!")


class CustomSDAThread(Thread):
    def __init__(self, target, args=None):
        self.target = target
        self.args = args
        self.result = None
        super().__init__()

    def run(self):
        """
        Calls the target function and passes the necessary arguments into the function.
        """
        self.result = self.target(self.args)

    def join(self, timeout=None):
        super().join(timeout)
        return self.result


email_sda_thread = CustomSDAThread(target=Util.is_valid_email, args="rafi.bello@yahoo.com")
password_sda_thread = CustomSDAThread(target=Util.is_valid_password, args="Python")  # writing the characters of the string

password_sda_thread.start()
email_sda_thread.start()

print(password_sda_thread.join())
print(email_sda_thread.join())


print("File has reached the end.")
