"""
You can create your own exception class in python. To do so:
1. Define your class and inherit from python's `Exception` class.
2. specify the details you want your exception class to contain (Things that make your exception specific)
3. Specify a custom message for your exception class.
"""

class CustomError(Exception):
    pass


class NotAcademyError(CustomError):
    """
    Handles errors related to Academies.
    """
    def __init__(self, message="Not an Academy"):
        super().__init__(message)
        # super().__init__("The given school name doesn't seem to be an Academy.")


class NotStudentError(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(f"{kwargs['student_name']} appears to not be a student. \nOnly students can go to school.")


class HumanBeing:
    def __init__(self, name: str, is_student: bool =False):
        self.name = name
        self.is_student = is_student

    def start_walking(self):
        return "Hey guys! I can walk. I am walking very slowly."

    def start_eating(self):
        return "Hey guys! I can eat. I am eating very slowly."

    def go_to_school(self, name_of_school: str) -> str:
        if not self.is_student:
            raise NotStudentError(student_name=self.name)
        if not isinstance(name_of_school, str):
            raise ValueError
        if "Academy" not in name_of_school:
            raise NotAcademyError(f"Looked for Academy in {name_of_school}, it was not found")
        return f"Hey guys! I school at {name_of_school}. I am learning very slowly. I don't seem to like school"


human_object = HumanBeing(name="Rafihatu", is_student=False)
print(human_object.go_to_school("Software Development"))
