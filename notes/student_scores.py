def find_top_students(student_scores):
    courses = list(list(student_scores.values())[0].keys())
    dicty = dict()
    for course in courses:
        students_and_their_scores = [(student, student_scores[student][course]) for student in student_scores]
        sorted_student_scores = sorted(students_and_their_scores, key=lambda tup: tup[1])
        top_students = [name for name,score in sorted_student_scores if score == sorted_student_scores[-1][1]]
        dicty[course] = top_students if len(top_students) > 1 else top_students[0]
    return dicty


student_scores = {
    "David": {"python": 89, "agile": 75, "frontend": 20, "scrum": 100},
    "Josiah": {"python": 10, "agile": 75, "frontend": 70, "scrum": 100},
    "Mark": {"python": 24, "agile": 85, "frontend": 72, "scrum": 50},
    "Jonathan": {"python": 95, "agile": 65, "frontend": 27, "scrum": 45},
    "Samson": {"python": 79, "agile": 10, "frontend": 80, "scrum": 80},
}


# print(find_top_students(student_scores))

# {'python': 'David', 'agile': 'Mark', 'frontend': ['Josiah', 'Mark']}