"""
Decorators in Python are based primarily on two assumptions:

a function may take another function as an argument,
you can create a function inside another function.
"""

def validate_card(func):
    def wrapper(card_number):
        """
        implement this function to only return the type of card if luhn's algorithm is true for the card number.
        :param card_number: card number
        :return: up to you to implement
        """
        pass
    return wrapper

def check_luhn_algorithm(card_number):
    """
    checks the card against luhn algorithm
    :param card_number: the card to be checked.
    :return: VALID, INVALID
    """
    pass

@validate_card
def check_card_type(card_number):
    """
    Checks if a card is AMEX, Master or VISA
    :param card_number: the card to be checked
    :return: Card type (AMEX, VISA, MASTER)
    """
    pass



validate_card()
import datetime


def get_current_time():
    current_date_and_time = str(datetime.datetime.now()).split()
    current_time = current_date_and_time[1]
    current_time = current_time[:2]
    return current_time

# def get_day_time(current_time):
#     if current_time >= "01" and current_time < "12":
#         return "Morning"
#     elif current_time > "12" and current_time < "18":
#         return "Afternoon"
#     else:
#         return "Evening"


#
# def get_daytime(func):
#     now = get_current_time()
#     def wrapper():
#         if now >= "01" and now < "12":
#             return func("Morning")
#         elif now > "12" and now < "18":
#             return func("Afternoon")
#         else:
#             return func("Evening")
#     return wrapper
#
#
# @get_daytime
# def greet(time_of_day: str = None):
#     return f"Good {time_of_day}"
#
#
# print(greet())


# def outer_function(expected_function):
#     def wrapper():
#         print("I was executed inside the decorator")
#         expected_function()
#         print("This is the end of the decorator")
#     return wrapper
#
#
# @outer_function
# def main_function():
#     print("Hi! I am the starter function!")
#
#
# print(outer_function(main_function))
# def prevent_zero_division(func):
#     def inner_func(first_num, second_num):
#         if second_num == 0:
#             return "Cannot divide by 0, please choose another number"
#         else:
#             return func(first_num, second_num)
#     return inner_func
#
#
# @prevent_zero_division
# def divide(first_num, second_num):
#     return first_num // second_num
#
#
# print(divide(5, 5))

def lowercase(func):
    def wrapper(word):
        print("Lower decorator called")
        return func(word).lower()
    return wrapper


def uppercase(func):
    def wrapper(word):
        print("Upper decorator called")
        return func(word).upper()
    return wrapper


@lowercase
@uppercase
def greet(word):
    return word

# lowercase(uppercase(greet("Students!"))))
print(greet("Students!"))



def prevent_spies(spy_type):
    def wrapper(func):
        """
        decorator function to validate login function
        :return: validated login function
        """
        def inner_func(username, password):
            return func(username, password)

    return wrapper


@prevent_spies(spy_type="hacker")
def login(username: str, password: str):
    """
    Complete this function so that a student logs in to the class.
    Note that username and password should be accepted in lowercase only.
    :param username: The first name of a known student of python ee11 class
    :param password: The surname of the student attempting to login.
    :return: "LOGIN SUCCESS" or "ERROR"
    """
    return "LOGIN SUCCESS"

# login("rafihatu", "bello")
prevent_spies(login("1", "2"))


# -----------------28/05/2022---------------------
