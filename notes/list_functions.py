students_list = ["Student1", "Student2", "Student3", "Student4",
                 "Student5", "Student6", "Student7", "Student8"]
extra_list = ["Student9", "Student10", "Student11", "Student12", "Student1"]

# 1. Lists allow duplicate values
# print(students_list)

# 2. You can get the value based on indexing.
# print(students_list[-1])

# 3. You can slice lists and strings by their index
# Slicing takes three parameters when using bracket notation which are -> [start: stop: step]
# print(students_list[::-1])

# 4. Lists are changeable
# You can change values based on their index
# students_list[0] = "First value"
# print(students_list)

# 5. Lists can be increased and can contain items of different types.
# - You can add items to the end of a list
# students_list.append(9)
# students_list.append(extra_list)
# students_list.append("Last Student")
# print(students_list)

# 6. You can insert into a list at a particular position.
# students_list.insert(4, "Inserted Student")
# print(students_list.index("Inserted Student"))
# print(students_list)

# 5. You can extend a list
# students_list.extend(extra_list)
# print(students_list)

# 6. Lists can be sorted in python:
disordered_list = ["Zack", "Anna", "Ella", "Faith", "Lord"]
numerical_disordered = [12, 44, 10, 5, 2, 8]
#
# disordered_list.sort(reverse=True)
# print(numerical_disordered)

# 7. Items can be removed from lists
# disordered_list.remove("Zack")
# print(disordered_list)

# 8. You can remove elements from a list based on their position
disordered_list.pop(2)
# print (disordered_list)