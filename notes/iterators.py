"""
Custom Iterators in Python.
"""
from math import sqrt

even_list = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
even_range = range(2, 21, 2)

iterator_obj = iter(student_generator)
print(next(iterator_obj))  # Ada
print(next(iterator_obj))  # Uju
print(next(iterator_obj))  # Chioma


def is_prime(n):
    for i in range(2, int(sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True


class PrimeIterator:
    # Iterator that allows you to iterate over n primes
    def __init__(self, n):
        self.n = n
        self.generated_numbers = 0
        self.number = 1

    def __iter__(self):
        return self

    def __next__(self):
        self.number += 1
        if self.generated_numbers >= self.n:
            raise StopIteration
        elif is_prime(self.number):
            self.generated_numbers += 1
            print(f"Total items returned so far: {self.generated_numbers}")
            return self.number
        return self.__next__()


# iter = PrimeIterator(1000000)
# print(next(iter))
# print(next(iter))
# print(next(iter))


class EvenNumbersIterator:
    def __init__(self):
        self.current_number = 1

    def __iter__(self):
        return self

    def __next__(self):
        self.current_number += 1
        if self.current_number % 2 == 0:
            return self.current_number
        return self.__next__()


# obj = EvenNumbersIterator()
# print(next(obj))


class OddNumbersIterator:
    def __init__(self):
        self.number = 0

    def __iter__(self):
        return self

    def __next__(self):
        self.number += 1
        if not self.number % 2 == 0:
            return self.number
        return self.__next__()


odd_obj = OddNumbersIterator()
# print(next(odd_obj))
# print(next(odd_obj))
# print(next(odd_obj))
# print(next(odd_obj))
# print(next(odd_obj))

example_string = {"first": 1, "second": 2, "third": 3, "fourth": 4}
string_obj = iter(example_string)
print(next(string_obj))
print(next(string_obj))
print(next(string_obj))
