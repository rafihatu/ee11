# def check_number_type(number):
#     def check_number_is_prime(num):
#         for n in range(2, int(num ** 0.5) + 1):
#             if (num % n == 0):
#                 return False
#         return True
#
#     def check_number_is_even_or_odd(num):
#         prime = check_number_is_prime(num)
#         if (num % 2) == 0 and prime:
#             return "even and prime"
#         if (num % 2) == 0 and not prime:
#             return "even and not prime"
#         if (num % 2) != 0 and prime:
#             return "odd and prime"
#         if (num % 2) != 0 and not prime:
#             return "even and not prime"
#
#     return check_number_is_even_or_odd(number)
#
#
# print(check_number_type(4))






"""
John got cakes of different sizes as a present from the employees at SDA for his birthday, 
each cake having non-negative integer size. (from size 1 and above)
Since he likes to make things perfect, he wants to arrange them from smallest to largest so that each cake will be bigger 
than the previous one exactly by 1 inch. He may need some additional cakes to be able to accomplish that. 
Help him figure out the minimum number of additional cakes needed.

Example
For cake_sizes = [6, 2, 3, 8], the output should be
solution(cake_sizes) = 3.
CN needs cakes of sizes 4, 5 and 7.
"""



# COMPREHENSIONS
# Comprehensions in Python provide us with a short and concise way to construct new sequences (such as lists, set, dictionary etc.)
# using sequences which have been already defined.
# Python supports the following 4 types of comprehensions:
# - List Comprehensions
# - Dictionary Comprehensions
# - Set Comprehensions
# - Generator Comprehensions think of this as tuple comprehension
# def solution(cake_sizes):
    # go through each cake within the range of the smallest cake size and the largest cake size in our list
    # for cake in range(min(cake_sizes), max(cake_sizes)):
    #     # if the cake size is not available in our cake sizes list,
    #     if cake not in cake_sizes:
    #         # add the cake to our extra cakes list
    #         extra_cakes.append(cake)
    # extra_cakes_tup = (cake for cake in range(min(cake_sizes), max(cake_sizes)) if cake not in cake_sizes)
    #
    # extra_cakes_set = {cake for cake in range(min(cake_sizes), max(cake_sizes)) if cake not in cake_sizes}

    # enum_cakes = list(enumerate(cake_sizes))
    # cakes_dict = dict()
    # for inner_tuple in enum_cakes:
    #     key = inner_tuple[0]
    #     value = inner_tuple[1]
    #     cakes_dict[key] = value

    # cakes_dict_comprehension = {inner_tuple[0]: inner_tuple[1] for inner_tuple in enum_cakes}
    # return tuple(extra_cakes_tup)
    # return extra_cakes_set, extra_cakes_list

# def solution1(cake_sizes):
#     cake_list = [cake for cake in cake_sizes if cake % 2 == 0]
#     print("cake_sizes:", cake_sizes)
#     print("cake_list:", cake_list)

# print(solution([6, 2, 3, 8]))

"""
list_comprehension_syntax = [value | for value in iterable |conditions]
value: what value you would like to add to your iterable
for loop: the regular `for` loop you would have used to iterate through a sequence
conditions: whatever conditions you want to set for adding values to the list
"""

# def solution(cake_sizes):
    # extra_cakes = []
    # count = 0
    # for cake in range(min(cake_sizes), max(cake_sizes)):
    #     if cake not in cake_sizes:
    #         count += 1
    #         # extra_cakes.append(1)
    # return sum(extra_cakes)
'''
Get the square of numbers within the range of 21
'''
example = [str(num) for num in range(21) if num % 2 == 0 and num != 0]
print(example)