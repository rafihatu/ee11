class FileManager():
    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode
        self.file = None

    def __enter__(self):
        # opening and sharing of resources
        self.file = open(self.filename, self.mode)
        return self.file

    def __exit__(self, err_type, err_value, err_traceback):
        if err_type:
            self.file.close()
            print("An error occured. Do you want anything else to happen?")
            return True


if __name__ == "__main__":
    with FileManager("test.txt", 'w') as file:
        file.write("Test")
        file[10]


# from contextlib import contextmanager
#
#
# @contextmanager
# def file_manager(file_name, mode):
#     file = open(file_name, mode)
#     yield file
#     file.close()
#
#
# if __name__ == "__main__":
#     with file_manager("test.txt", 'a') as file:
#         file.write("yielded function context manager.")


"""
With reference to yesterday's assignment, create your own context manager class 
that logs all people that tried to access a certain resource in your organisation.
Any one that tries to login, write the person's username, time of attempted login and return value of login (successful or error)
"""