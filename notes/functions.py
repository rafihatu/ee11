# DRY means Don't Repeat Yourself


"""
Description:
Everytime a trainer at SDA starts a class, we want the trainer to take attendance of students as they join the call.
We want the trainer to not go through the hard work of manually taking the attendance.
We also want the trainer to not start writing any code to handle attendance every day.
"""
def get_students_attendance():
    attendance_list = []

    while len(set(attendance_list)) < 15:
        try:
            student_name = input("Hi there, \nWhat is your name? ")
            attendance_list.append(student_name.capitalize())
        except Exception as my_error:
            print("Oh no! You did something wrong, try again!")
            print(my_error)
        else:
            print(f"The code ran without errors, The newly added name is {student_name}")
        finally:
            print("The finally block was successfully executed, hurray!")

    print(attendance_list)







# 1. def keyword is used to define a function.
# 2. Give your function a name.
# 3. Make sure to use the parenthesis immediately after the function name followed by your colon.
# 4. Do not forget to indent every code belonging to that function properly.







"""
Class Task: Create a function named "check_alphabet_group"
The purpose of your function is to check what letter a name starts with.
The letter can be in any case either upper or lower case.
Your function should request input from a user by asking a user for their name and 
tell the user what group they belong to based on the first letter of their name.
"""

def check_alphabet_group(**kwargs):
    greeting = f"Good {kwargs['time_of_day']}"
    group = f"You belong to group {kwargs['name'][0].upper()}"
    student_class = f"Your are a student of {kwargs['student_class']}"
    student_age = f"You are {kwargs['age']} year(s) old"
    return f"{greeting} \n{group} \n{student_class} \n{student_age}"

trainer_name = "Rafihatu"
my_age = 1
group = check_alphabet_group(name=trainer_name, age=my_age, student_class='EE11', time_of_day="Afternoon", learning_type="Remote_learning")
# group = check_alphabet_group(name="Rafihatu")

print(f"The trainer's details includes: \n{group}")


# keys_list = ["name", "age", "level"]
# values_list = ["Bello", 2, "EE11"]
#
# arguments = dict(zip(keys_list, values_list))
# check_alphabet_group(**arguments)

"""
Write a function `check_number_type` that takes a number and checks if the number is a prime number, odd number or even number.
If the number is an even number but not prime, your function should return "Even but not prime".
If the number is an odd number but not prime, your function should return "Odd but not prime".
If the number is an even number and prime, your function should return "Even and prime".
If the number is an odd number and prime, your function should return "Odd and prime".
"""