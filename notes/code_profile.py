import timeit
from oop import Person

statement = """Person(name="Anner", age=2, hair_color="brown", eye_color="black", nationality="Estonian")"""
setup = """import timeit
from oop import Person"""

print(timeit.timeit(stmt=statement, setup=setup))
