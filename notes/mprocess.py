from multiprocessing import Process
from regex import Util


email_process = Process(target=Util.is_valid_email, args=("rafihatu@sda.hq",))
password_process = Process(target=Util.is_valid_password, args=("Rafihatu01!",))


if __name__ == '__main__':
    email_process.start()
    password_process.start()

    email_process.join()
    password_process.join()

    print("This is the end of the if name main.")


"""
Using the two functions we previously created for our card validators, 
1. In a py file, separate the functions into individual threads and run them.
2. In a different py file, separate the functions into individual processes and run them.
"""
