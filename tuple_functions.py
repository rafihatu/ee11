students_list = ["Student1", "Student2", "Student3", "Student4",
                 "Student5", "Student6", "Student7", "Student8"]
extra_list = ["Student9", "Student10", "Student11", "Student12"]



students_tuple = tuple(students_list)
extra_tuple = tuple(extra_list)


# 1. They are ordered, they are unchangeable, allow duplicates, they can be gotten based on their index
# Get items based on their index
# print(students_tuple[::2])
# print(students_tuple.count("Student1"))
print(students_tuple.index("Student6"))