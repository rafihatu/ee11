from list_functions import students_list, extra_list


students_set = set(students_list)
extra_set = set(extra_list)

# 1. Sets do not allow duplicate values and they are unordered.
# print(students_set)

# 2. Because sets are unordered, indexing operations cannot be performed on them.
# print(students_set[0])  # throws an error

# 3. Set items are unchangeable.
# print(students_set)
# print(extra_set)

# print("Student12" in extra_set)
# if "Student12" in extra_set or "Student12" in students_set :
#     print("Student12 is present in class")

# for each_student in students_set:
#     print(each_student)


# 4. You can create new sets from joining two sets together.
# new_set = students_set.union(extra_set)
# print(len(new_set))

# 5. You can update sets.
# students_set.update(extra_set)
# print(len(students_set))

# 6. Check Difference between two sets
print(students_set)
# students_set.remove()