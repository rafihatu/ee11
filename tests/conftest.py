import pytest
from src.triangle.triangle import Triangle


@pytest.fixture(scope="session")
def triangle():
    triangle = Triangle(3, 4, 5)
    return triangle
