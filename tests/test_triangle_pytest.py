from src.triangle.triangle import Triangle


def test_when_sides_are_all_the_same_then_triangle_is_equilateral():
    triangle = Triangle(5, 5, 5)
    assert triangle.is_equilateral()
    assert not triangle.is_scalene()


def test_when_sides_are_not_the_same_then_triangle_is_scalene(triangle):
    assert triangle.is_scalene()


def test_when_sides_are_not_the_same_then_triangle_is_not_isosceles():
    triangle = Triangle(3, 4, 5)
    assert not triangle.is_isosceles()


def test_when_sides_are_not_the_same_then_triangle_is_not_equilateral():
    triangle = Triangle(3, 4, 5)
    assert not triangle.is_equilateral()


def test_when_sides_are_not_the_same_and_meets_pythagoras_theorem_then_triangle_is_right_angled():
    triangle = Triangle(3, 4, 5)
    assert triangle.is_right_angled()
