from classes import Calculator
from unittest import TestCase, mock


class CalculatorTests(TestCase):
    def setUp(self):
        self.calc = Calculator()

    def test_when_reset_function_is_called_then_calculator_value_is_zero(self):
        result = self.calc.reset_value()
        expected_value = 0
        self.assertEqual(result, expected_value)

    @mock.patch("builtins.input", return_value=0)
    def test_when_division_by_zero_then_zero_division_error_is_raised(self, mock_obj):
        self.calc.add(10)
        expected = "Not allowed to divide by zero."
        result = self.calc.divide()
        self.assertEqual(result, expected)

    def test_when_addition_function_is_called_then_returns_correct_result(self):
        result = self.calc.add(10)
        expected = 10
        self.assertEqual(result, expected)
        self.assertIsNotNone(result)
        self.assertEqual(result-1, 9)

    def test_when_subtraction_function_is_called_then_returns_correct_result(self):
        result = self.calc.subtract(10)
        self.assertEqual(result, -10)   # assert self.calc.subtract(10) == -10

    def test_when_addition_function_is_called_with_wrong_parameters_then_error_is_properly_handled(self):
        assert self.calc.add("ten") == 10

    def tearDown(self):
        del self.calc
