from classes import Calculator
import pytest


def test_when_addition_function_is_called_then_returns_correct_result():
    calc = Calculator()
    result = calc.add(10)
    expected = 10
    assert result == expected
    assert result is not None
    assert result - 1 == 9

def test_when_subtraction_function_is_called_then_returns_correct_result():
    calc = Calculator()
    assert calc.subtract(10) == -10

def test_when_addition_function_is_called_with_wrong_parameters_then_error_is_properly_handled():
    calc = Calculator()
    assert calc.add("ten") == 10

def test_when_addition_function_is_called_with_numerical_string_parameters_then_error_is_properly_handled():
    calc = Calculator()
    assert calc.add("10") == 10

def test_when_addition_function_is_called_with_non_numeric_parameters_then_error_is_properly_handled():
    calc = Calculator()
    with pytest.raises(ValueError):
        calc.add("rafis_number")
