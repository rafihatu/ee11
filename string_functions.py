first_name = "RAFIHATU"
last_name = "Bello"

# 1. You can get each letter in a string.
# for letter in first_name:
#     print(letter)

# 2. You can get a letter at a specific position.
# Get the first letter in a string:
first_letter = first_name[0]

# Indexing in Python starts at zero (0)
# '0 1 2 3 4 5 6 7'
# 'R a f i h a t u'
# '-7 -6 -5 -4 -3 -2 -1'


# 3. You can add multiple strings to one another.
introduction = f"My name is {first_name} {last_name}"
other_way = "My name is " + first_name + " " + last_name
# print(other_way)

# 4. You can replace values in a string:
# first_name = first_name.replace("a", "e")
# last_name = last_name.replace("l", "k")
print(last_name)
# 5. Get the length of a string:
# print(len(last_name))

# 6. Check if certain words are in a string
# Check that ny name is in Introduction:
# if "Rafihatu" in introduction:
#     print("Yeah, your name is in Introduction!")
# else:
#     print("Sorry, not found")


# 7. Check that a string startswith or endswith certain characters.
# if first_name.startswith("R"):
#     print("Yes, it ends with hatu")
# else:
#     print("It doesn't end with hatu")

# 8. Check case of string if in lower case, upper case or mixed:
print(first_name.capitalize())
print(first_name.lower())
print(first_name.upper())
# if first_name == first_name.capitalize(): #Rafihatu
#     print("You entered your name correctly")
# else:
#     print("Your name must start with a capital letter")

