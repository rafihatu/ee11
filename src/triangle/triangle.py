"""
Task: Create a triangle class. Your triangle class should have three default values.
Your triangle class should have methods `get_base, get_height, get_area, is_`.

"""

class Triangle:
    def __init__(self, first: int, second: int, third: int):
        self.first_side = first
        self.second_side = second
        self.third_side = third

    def get_area(self):
        semi = self.calculate_semi_perimeter()
        area = sqrt(semi * (semi - self.first_side) * (semi - self.second_side) * (semi - self.third_side))
        return ('%0.2f' %area)

    def is_isosceles(self):
        if (
            (self.first_side == self.second_side and self.third_side < self.first_side) or
            (self.first_side == self.third_side and self.second_side < self.first_side) or
            (self.second_side == self.third_side and self.first_side < self.second_side)
        ):
            return True
        return False

    def is_equilateral(self):
        return self.first_side == self.second_side == self.third_side

    def is_scalene(self):
        return self.first_side != self.second_side != self.third_side

    def calculate_semi_perimeter(self):
        semi_perimeter = (self.first_side + self.second_side + self.third_side) / 2
        return semi_perimeter

    def is_right_angled(self):
        sides = [self.first_side, self.second_side, self.third_side]
        hypothenuse = max(sides)
        sides.remove(hypothenuse)
        if (hypothenuse**2) == (sides[0]**2) + (sides[1]**2):
            return True
        return False


triangle = Triangle(3, 4, 5)
print(triangle.is_scalene())